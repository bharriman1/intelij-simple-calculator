package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class Controller {
    @FXML
    private Button goButton;
    @FXML
    private Button clearButton;
    @FXML
    private TextField postfixField;
    @FXML
    private TextField resultField;
    @FXML
    private Label errorLabel;
    @FXML
    void handleGoButton(ActionEvent e)
    {
        String postfix = postfixField.getText();
        try
        {
            Postfix p = new Postfix(postfix);
            resultField.setText("" + p.eval());
            errorLabel.setVisible(false);
        }
        catch(NumberFormatException nfe)
        {
            errorLabel.setVisible(true);
        }
    }
    @FXML
    void handleClearButton(ActionEvent e)
    {
        postfixField.setText("");
        resultField.setText("");
        errorLabel.setVisible(false);
    }
}
