package sample;
import java.util.Stack;

public class Postfix
{
    private String expr;

    public Postfix(String e)
    {
        expr = e;
    }

    public double eval()
    {
        String[] tokens = expr.split(" ");
        Stack<Double> s = new Stack<Double>();
        Stack<Double> abstack = new Stack<Double>();
        boolean abs = false;

        for (String token : tokens)
        {
            double x, y;
            switch(token)
            {
                case "|":
                    if(abs == true)
                    {
                        s.push(Math.abs(abstack.pop()));
                        abstack.clear();
                    }
                    abs = !abs;
                    break;
                case "+":
                    if(abs)
                    {
                        x = abstack.pop();
                        y = abstack.pop();
                        abstack.push(x+y);
                    }
                    else
                    {
                        x = s.pop();
                        y = s.pop();
                        s.push(x+y);
                    }
                    break;
                case "-":
                    if(abs)
                    {
                        x = abstack.pop();
                        y = abstack.pop();
                        abstack.push(x-y);
                    }
                    else
                    {
                        x = s.pop();
                        y = s.pop();
                        s.push(x-y);
                    }
                    break;
                case "*":
                    if(abs)
                    {
                        x = abstack.pop();
                        y = abstack.pop();
                        abstack.push(x*y);
                    }
                    else
                    {
                        x = s.pop();
                        y = s.pop();
                        s.push(x*y);
                    }
                    break;
                case "/":
                    if(abs)
                    {
                        x = abstack.pop();
                        y = abstack.pop();
                        abstack.push(x/y);
                    }
                    else
                    {
                        x = s.pop();
                        y = s.pop();
                        s.push(x/y);
                    }
                    break;
                case "^":
                    if(abs)
                    {
                        x = abstack.pop();
                        y = abstack.pop();
                        abstack.push(Math.pow(y, x));
                    }
                    else
                    {
                        x = s.pop();
                        y = s.pop();
                        s.push(Math.pow(y, x));
                    }
                    break;
                case "sqrt":
                    if(abs)
                    {
                        x = abstack.pop();
                        abstack.push(Math.sqrt(x));
                    }
                    else
                    {
                        x = s.pop();
                        s.push(Math.sqrt(x));
                    }
                    break;
                case "log":
                    if(abs)
                    {
                        x = abstack.pop();
                        abstack.push(Math.log10(x));
                    }
                    else
                    {
                        x = s.pop();
                        s.push(Math.log10(x));
                    }
                    break;
                case "ln":
                    if(abs)
                    {
                        x = abstack.pop();
                        abstack.push(Math.log(x));
                    }
                    else
                    {
                        x = s.pop();
                        s.push(Math.log(x));
                    }
                    break;
                case "sin":
                    if(abs)
                    {
                        x = abstack.pop();
                        abstack.push(Math.sin(x));
                    }
                    else
                    {
                        x = s.pop();
                        s.push(Math.sin(x));
                    }
                    break;
                case "cos":
                    if(abs)
                    {
                        x = abstack.pop();
                        abstack.push(Math.cos(x));
                    }
                    else
                    {
                        x = s.pop();
                        s.push(Math.cos(x));
                    }
                    break;
                case "tan":
                    if(abs)
                    {
                        x = abstack.pop();
                        abstack.push(Math.tan(x));
                    }
                    else
                    {
                        x = s.pop();
                        s.push(Math.tan(x));
                    }
                    break;
                case "sec":
                    if(abs)
                    {
                        x = abstack.pop();
                        abstack.push((1/(Math.cos(x))));
                    }
                    else
                    {
                        x = s.pop();
                        s.push((1/(Math.cos(x))));
                    }
                    break;
                case "csc":
                    if(abs)
                    {
                        x = abstack.pop();
                        abstack.push((1/(Math.sin(x))));
                    }
                    else
                    {
                        x = s.pop();
                        s.push((1/(Math.sin(x))));
                    }
                    break;
                case "cot":
                    if(abs)
                    {
                        x = abstack.pop();
                        abstack.push((1/(Math.tan(x))));
                    }
                    else
                    {
                        x = s.pop();
                        s.push((1/(Math.tan(x))));
                    }
                    break;
                case "π":
                    if(abs)
                    {
                        abstack.push(Math.PI);
                    }
                    else
                    {
                        s.push(Math.PI);
                    }
                    break;
                case "e":
                    if(abs)
                    {
                        abstack.push(Math.E);
                    }
                    else
                    {
                        s.push(Math.E);
                    }
                    break;
                case "!":
                    if(abs)
                    {
                        x = abstack.pop();
                        for (double i=x; i>1; i--)
                        {
                            double z = (i-1);
                            x = x * (z);
                        }
                        abstack.push(x);
                    }
                    else
                    {
                        x = s.pop();
                        for (double i=x; i>1; i--)
                        {
                            double z = (i-1);
                            x = x * (z);
                        }
                        s.push(x);
                    }
                    break;
                case "%":
                    if(abs)
                    {
                        x = abstack.pop();
                        y = abstack.pop();
                        abstack.push(y%x);
                    }
                    else
                    {
                        x = s.pop();
                        y = s.pop();
                        s.push(y%x);
                    }
                    break;
                default:
                    if(abs)
                    {
                        abstack.push(Double.parseDouble(token));
                    }
                    else
                    {
                        s.push(Double.parseDouble(token));
                    }
                    break;
            }
        }
        return s.pop();
    }
}